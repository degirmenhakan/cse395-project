#-------------------------------------------------
#
# Project created by QtCreator 2014-11-26T13:23:47
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Proje
TEMPLATE = app


SOURCES += main.cpp\
        interface.cpp \
    about.cpp \
    sound_treads.cpp \


HEADERS  += interface.h \
    about.h \
    sound_treads.h

FORMS    += interface.ui

INCLUDEPATH += C:/opencv/build/include/

    #debug libs
LIBS +=-LC:/opencv/build/x86/vc11/staticlib\
    #+=-LC:/opencv249/build/x86/vc11/lib\
 LIBS +=   -lopencv_core249d\
 LIBS +=   -lopencv_highgui249d\
LIBS +=    -lIlmImfd\
 LIBS +=   -llibpngd\
  LIBS +=  -llibjpegd\
  LIBS +=  -llibjasperd\
  LIBS +=  -lIlmImfd\     
 LIBS +=   -lzlibd


#