#include "interface.h"
#include "about.h"
#include "sound_treads.h"
#include <QKeyEvent>
#include <iostream>
#include <cv.h>
#include <highgui.h>
#include<pthread.h>
#include"rs232.h"
#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>
using namespace aruco;
using namespace cv;
using namespace std;

Interface::Interface(QWidget *parent)
{

    tabWidget = new QTabWidget;
    tabWidget->addTab(new MainWindow, tr("Main Menu"));
    tabWidget->addTab(new AboutWindow, tr("About Menu"));
    //tabWidget->addTab(new ApplicationsTab(fileInfo), tr("Applications"));

    /*buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));*/

    closeButton = new QPushButton("Close",this);
    closeButton->setObjectName(QString::fromUtf8("Close"));
    connect( closeButton, SIGNAL(clicked()), this, SLOT(close()));



    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
    mainLayout->addWidget(tabWidget);
    mainLayout->addWidget(closeButton);
    setLayout(mainLayout);

    setWindowTitle(tr("Bateri Simulation"));
}

MainWindow::MainWindow( QWidget *parent )
{
    player = new QMediaPlayer;
    playGif = new QMovie(this);
    playGif->setCacheMode(QMovie::CacheAll);

    player->setMedia(QUrl::fromLocalFile("/home/sefatascioglu/workspace/ProjeDemoSon/background music/Background Music.mp3"));
    player->setVolume(100);
    player->play();

    createGifPart();
    createButtonsPart();
    createSettingsPart();
    createKaraokeSettingsPart();
    createRecordPart();

    QGridLayout *mainLayout;

    mainLayout = new QGridLayout;

    mainLayout->addWidget(GifState,0,0,2,2);
    mainLayout->addWidget(ButtonState,2,0);
    mainLayout->addWidget(SettingsState,2,1);
    mainLayout->addWidget(RecordState,3,0);
    mainLayout->addWidget(KaraokeState,3,1);
    mainLayout->setRowStretch(1, 1);
    mainLayout->setRowStretch(2, 1);
    mainLayout->setColumnStretch(0, 1);
    mainLayout->setColumnStretch(1, 1);
    setLayout(mainLayout);

}

void MainWindow::createButtonsPart()
{
    QSize iconSize(375, 70);

    ButtonState = new QGroupBox(tr("Game Menu"));

    buttonsLayout = new QVBoxLayout;

    gamePlayButton = new QToolButton;
    gamePlayButton->setIcon(QIcon("/home/sefatascioglu/workspace/ProjeDemoSon/play-button.jpg"));
    gamePlayButton->setIconSize(iconSize);
    gamePlayButton->setToolTip(tr("Stop"));
    connect(gamePlayButton, SIGNAL(clicked()), this, SLOT(showPlayWindow()));

    //Burası Yazılımcıların Programını Çağıracak..

    /*About = new QPushButton("About",this);
    About->setObjectName(QString::fromUtf8("About"));
    connect( About, SIGNAL(clicked()), this, SLOT(showAboutWindow()));

    Close = new QPushButton("Close",this);
    Close->setObjectName(QString::fromUtf8("Close"));
    connect( Close, SIGNAL(clicked()), this, SLOT(close()));*/


    buttonsLayout->addWidget(gamePlayButton);
    //buttonsLayout->addWidget(About);
    //buttonsLayout->addWidget(Close);
    buttonsLayout->addStretch(1);
    ButtonState->setLayout(buttonsLayout);
}

void MainWindow::createSettingsPart()
{

    SettingsState = new QGroupBox(tr("Settings Menu"));

    settingsLayout = new QVBoxLayout;

    volumeStatusBar = new QScrollBar(this);
    volumeStatusBar->setObjectName(QString::fromUtf8("volumeStatusBar"));
    volumeStatusBar->setOrientation(Qt::Horizontal);
    volumeStatusBar->setValue(100);
    connect( volumeStatusBar, SIGNAL(valueChanged(int)), this, SLOT(setVolumeLevel(int)) );

    muteBox = new QCheckBox(this);
    muteBox->setObjectName(QStringLiteral("muteBox"));
    muteBox->setText("Turn of background music");
    connect( muteBox, SIGNAL(toggled(bool)), this, SLOT(operateBackMusic(bool)));


    volumeText = new QLabel(this);
    volumeText->setObjectName(QStringLiteral("volumeText"));
    volumeText->setText("Volume");

    settingsLayout->addWidget(volumeText);
    settingsLayout->addWidget(volumeStatusBar);
    settingsLayout->addWidget(muteBox);
    settingsLayout->addStretch(1);
    SettingsState->setLayout(settingsLayout);
}


void MainWindow::operateBackMusic(bool checkbox)
{
    if(checkbox==true)
        player->pause();
    else
        player->play();

}
void MainWindow::createRecordPart()
{
    QSize iconSize(25, 25);

    RecordState = new QGroupBox(tr("Record Menu"));
    QString recDirDefaultPath = "/home/sefatascioglu/workspace/ProjeDemoSon/";

    recordLayout = new QHBoxLayout(RecordState);

    audioRecorder = new QAudioRecorder;
    audioRecorder->setOutputLocation(QUrl::fromLocalFile(recDirDefaultPath));

    recStartButton = new QToolButton;
    recStartButton->setIcon(QIcon("/home/sefatascioglu/workspace/ProjeDemoSon/record-button.jpg"));
    recStartButton->setIconSize(iconSize);
    recStartButton->setToolTip(tr("Stop"));
    connect(recStartButton, SIGNAL(clicked()), audioRecorder, SLOT(record()));

    recStopButton = new QToolButton;
    recStopButton->setIcon(QIcon("/home/sefatascioglu/workspace/ProjeDemoSon/stop-button.jpg"));
    recStopButton->setIconSize(iconSize);
    recStopButton->setToolTip(tr("Stop"));
    connect(recStopButton, SIGNAL(clicked()), audioRecorder, SLOT(stop()));

    directoryComboBox = new QComboBox;
    directoryLabel = new QLabel(tr("In directory:  "));

    browseButton = new QToolButton;
    browseButton->setIcon(style()->standardIcon(QStyle::SP_DialogOpenButton));
    browseButton->setIconSize(iconSize);
    connect(browseButton, SIGNAL(clicked()), this, SLOT(browse()));

    directoryComboBox->setEditable(true);
    directoryComboBox->addItem("/home/sefatascioglu/workspace/ProjeDemoSon/");
    connect(directoryComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(setDirPath(QString)));

    recordLayout->addWidget(recStartButton);
    recordLayout->addWidget(recStopButton);
    recordLayout->addWidget(directoryLabel);
    recordLayout->addWidget(directoryComboBox);
    recordLayout->addWidget(browseButton);
    recordLayout->addStretch(1);
    RecordState->setLayout(recordLayout);
}
void MainWindow::setDirPath(QString dirPath)
{
    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/amr");
    audioSettings.setQuality(QMultimedia::HighQuality);

    audioRecorder->setEncodingSettings(audioSettings);

    audioRecorder->setOutputLocation(QUrl::fromLocalFile(dirPath));
}

void MainWindow::browse()
{
    QString recDirPath = QFileDialog::getExistingDirectory(this,
                                    tr("Find Files"), "/home/sefatascioglu/workspace/ProjeDemoSon/");

    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/amr");
    audioSettings.setQuality(QMultimedia::HighQuality);

    audioRecorder->setEncodingSettings(audioSettings);

    audioRecorder->setOutputLocation(QUrl::fromLocalFile(recDirPath));
}

void MainWindow::createKaraokeSettingsPart()
{
    QSize iconSize(25, 25);

    KaraokeState = new QGroupBox(tr("Karaoke Settings"));

    karaokeSettingsLayout = new QHBoxLayout(KaraokeState);

    openButton = new QToolButton;
    openButton->setIcon(style()->standardIcon(QStyle::SP_DialogOpenButton));
    openButton->setIconSize(iconSize);
    openButton->setToolTip(tr("Open File"));
    connect(openButton, SIGNAL(clicked()), this, SLOT(open()));

    playButton = new QToolButton;
    playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    playButton->setIconSize(iconSize);
    playButton->setToolTip(tr("Play"));
    connect(playButton, SIGNAL(clicked()), player, SLOT(play()));

    pauseButton = new QToolButton;
    pauseButton->setCheckable(true);
    pauseButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    pauseButton->setIconSize(iconSize);
    pauseButton->setToolTip(tr("Pause"));
    connect(pauseButton, SIGNAL(clicked(bool)), player, SLOT(pause()));

    stopButton = new QToolButton;
    stopButton->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    stopButton->setIconSize(iconSize);
    stopButton->setToolTip(tr("Stop"));
    connect(stopButton, SIGNAL(clicked()), player, SLOT(stop()));

    karaokeSettingsLayout->addStretch();
    karaokeSettingsLayout->addWidget(openButton);
    karaokeSettingsLayout->addWidget(playButton);
    karaokeSettingsLayout->addWidget(pauseButton);
    karaokeSettingsLayout->addWidget(stopButton);
    karaokeSettingsLayout->addStretch(1);
    KaraokeState->setLayout(karaokeSettingsLayout);


}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open a Music"),
                              "/home/sefatascioglu/workspace/ProjeDemoSon/",
                              tr("Music Files (*.mp3 *.waw)"));
    if (!fileName.isEmpty())
        openFile(fileName);
}

void MainWindow::openFile(const QString &fileName)
{
    currentMusicDirectory = QFileInfo(fileName).path();

    player->setMedia(QUrl::fromLocalFile(fileName));
    player->setVolume(50);
    player->play();
}

void MainWindow::createGifPart()
{
    GifState =new QWidget(this);
    GifState->setGeometry(QRect(0, 0, 650, 366));

    gifLayout = new QGridLayout(GifState);

    QString gifPath = "/home/sefatascioglu/workspace/ProjeDemoSon/gifs/gif-drummer-alexis-von-kraven.gif";

    gifLabel = new QLabel(this);
    gifLabel->setObjectName(QStringLiteral("gifLabel"));
    gifLabel->setScaledContents(true);

    gifLabel->setMovie(playGif);
    playGif->setFileName(gifPath);
    playGif->start();

    gifLayout->addWidget(gifLabel);

}

void MainWindow::setVolumeLevel(int level)
{
    player->setVolume(level);
}

/*void MainWindow::showAboutWindow()
{
    AboutWindow *other = new AboutWindow;
         //other->move(x() + 40, y() + 40);
    //this->hide();

    other->resize(411, 420);
    other->move(25, 25);
    other->setWindowTitle("Bateri Simulation");
    other->show();

}*/

void MainWindow::showPlayWindow()
{
    /*(void)QMessageBox::information(this,tr("Play Menu"),
                                        tr("Go to play game function.."),QMessageBox::Cancel);*/

    playButton->setEnabled(false);

    //serPort=fopen("/dev/ttyACM0","r");

    port.OpenComport(0,9600);

    drum_1.name = "drum_1";
    drum_1.soundDirPath = "/home/sefatascioglu/workspace/ProjeDemoSon/sounds/drum_1.mp3";


    drum_2.name = "drum_1";
    drum_2.soundDirPath = "/home/sefatascioglu/workspace/ProjeDemoSon/sounds/drum_2.mp3";


    pedal.name = "pedal";
    pedal.soundDirPath = "/home/sefatascioglu/workspace/ProjeDemoSon/sounds/pedal.mp3";

    ring_1.name = "ring_1";
    ring_1.soundDirPath = "/home/sefatascioglu/workspace/ProjeDemoSon/sounds/ring_1.mp3";

    ring_2.name = "ring_2";
    ring_2.soundDirPath = "/home/sefatascioglu/workspace/ProjeDemoSon/sounds/ring_2.mp3";

    /*if(!serPort){
        QMessageBox FileMessage;
        FileMessage.setText("Device cannot connected.");
        FileMessage.setStandardButtons(QMessageBox::Ok);
        FileMessage.exec();
        setVisible(false);
        exit(1);

    }*/
    playBattery();
    //fclose(serPort);
}

int MainWindow::playBattery()
{


    VideoCapture cap(0); //capture the video from web cam


    MarkerDetector MDetector;
    vector<Marker> Markers;




    if ( !cap.isOpened() )  // if not success, exit program
    {
            cout << "Cannot open the web cam" << endl;
            return -1;
    }


   while (1) {


       Mat imgOriginal;
       Mat src1,dst;

       src1 = imread("/home/sefatascioglu/Resimler/drumPic2.jpg");

       bool bSuccess = cap.read(imgOriginal); // read a new frame from video

        if (!bSuccess) //if not success, break loop
       {
            cout << "Cannot read a frame from video stream" << endl;
            break;
       }
        //imshow("Sefa",imgOriginal);
        //detect
        //MDetector.detect(imgOriginal,Markers);
        MDetector.detect(imgOriginal,Markers);

        //for each marker, draw info and its boundaries in the image
       for (unsigned int i=0;i<Markers.size();i++) {

                   //cout<<Markers[i]<<endl;

                   Point p= Markers[i].getCenter();
                   circle(src1,Point(640-p.x,p.y),15, Scalar(0,255,0), -1,8,0);

                   if((p.x >460 && p.x < 585)&&(p.y > 300 && p.y < 350)){
                               ring_1.start();
                   }

                   else if((p.x > 340 && p.x < 430)&&(p.y > 245 && p.y < 305 )){
                               drum_1.start();
                    }

                   else if((p.x > 208 && p.x < 308)&&(p.y > 237 && p.y < 297 )){
                               drum_2.start();
                   }

                   else if((p.x > 30 && p.x < 180)&&(p.y > 260 && p.y < 320)){
                              ring_2.start();
                   }

                   circle(src1,Point(640-p.x,p.y),15, Scalar(0,255,0), -1,8,0);
       }


           //printf("Ball! x=%f y=%f r=%f\n\r", p[0], p[1], p[2]);

           /*if((p[0] > 51 && p[0] < 180)&&(p[1] > 300 && p[1] < 375)){

               cout<<"abi ben 1. zilim"<<endl;
           }

           if((p[0] > 450 && p[0] < 631)&&(p[1] > 108 && p[1] < 276+108))
               cout<<"abi ben 2.zilim"<<endl;

           if((p[0] > 205 && p[0] < 300)&&(p[1] > 131+108 && p[1] < 223+108))
               cout<<"abi ben 1.davulum"<<endl;

           if((p[0] > 325 && p[0] < 430)&&(p[1] > 120+108 && p[1] < 210+108))
               cout<<"abi ben 2. davulum"<<endl;
*/


           addWeighted(imgOriginal, 0.001, src1, 0.9, 0.0, dst);
           putText(dst, "GROUP-8 DRUM MASTER", cvPoint(200,100),
               FONT_HERSHEY_COMPLEX_SMALL, 0.9, cvScalar(150,200,250), 1.2, CV_AA);
           imshow("Image", dst);

           if ((cvWaitKey(10) & 255) == 27){
                       break;
                   }
       }
return 0;
}




