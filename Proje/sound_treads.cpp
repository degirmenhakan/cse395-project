#include "sound_treads.h"
//#include "sefa.h"
#include <QtCore>
#include <QDebug>


Sound_Treads::Sound_Treads()
{
    player = new QMediaPlayer;
}

void Sound_Treads::run()
{
    //qDebug() << "a";
    player->stop();
    player->setMedia(QUrl::fromLocalFile(this->soundDirPath));
    player->setVolume(100);
    player->play();

}
