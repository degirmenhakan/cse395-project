#ifndef SOUND_TREADS_H
#define SOUND_TREADS_H

#include <QtCore>
#include <QMediaPlayer>
#include <QString>

class Sound_Treads: public QThread
{
public:
    Sound_Treads();
    void run();
    QString name;
    QString soundDirPath;

private:
    QMediaPlayer *player;

};

#endif // SOUND_TREADS_H
